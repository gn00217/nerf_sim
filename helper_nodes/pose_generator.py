import rclpy
from rclpy.node import Node
import json
import numpy as np

from geometry_msgs.msg import Pose
from scipy.spatial.transform import Rotation as R

# Pose Generation is for Testing Purposes Only - Takes pose from file, converts to msg format and publishes it to topic

class MinimalPublisher(Node):

    def __init__(self):
        super().__init__('minimal_publisher')
        self.publisher_ = self.create_publisher(Pose, 'pose', 1)
        timer_period = 1  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.i = 0

        orig = open('/workspace/ros2_iron/src/nerf_sim/test_materials/left_fisheye.json', 'r')
        self.orig_json = json.load(orig)
        
        # testing using a single pose only
        # self.pose = np.matrix(orig_json['camera_path'][0]['camera_to_world'])

    def timer_callback(self):
        self.pose = np.matrix(self.orig_json['camera_path'][self.i]['camera_to_world'])
        
        msg = Pose()
        msg.position.x = self.pose[0,3]
        msg.position.y = self.pose[1,3]
        msg.position.z = self.pose[2,3]
        
        rot_mat = self.pose[0:3,0:3]
        r = R.from_matrix(rot_mat)
        quat = r.as_quat()
        
        msg.orientation.x = quat[0]
        msg.orientation.y = quat[1]
        msg.orientation.z = quat[2]
        msg.orientation.w = quat[3]
        self.publisher_.publish(msg)
        self.get_logger().info('Publishing: "%s"' % msg)
        
        if self.i < len(self.orig_json['camera_path']):
            self.i+=1


def main(args=None):
    rclpy.init(args=args)

    minimal_publisher = MinimalPublisher()

    rclpy.spin(minimal_publisher)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    minimal_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()