import rclpy
from rclpy.node import Node

from cv_bridge import CvBridge
from sensor_msgs.msg import Image
from geometry_msgs.msg import Pose
from scipy.spatial.transform import Rotation as R
from nerf_sim.nerf_render import RenderCameraPath

import cv2
import json
import numpy as np
import mediapy as media


# subscribe to pose coming in from topic
# write out config file for nerf
# run nerf to get image
# publish image to topic

class DepthGenerator(Node):
    def __init__(self):
        super().__init__('minimal_publisher')
        # subscribe to pose coming in from topic
        self.subscription = self.create_subscription(Pose, 'pose', self.listener_callback, 10)
        self.subscription
        
        self.publisher_rgb = self.create_publisher(Image, 'camera/depth', 10)
        timer_period = 10  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.i = 0
        self.bridge = CvBridge()
        self.img_depth = None
                
        self.render = RenderCameraPath()

    def listener_callback(self, data):
        matrix = np.matrix(np.zeros((4,4)))
        
        # convert to 4x4 matrix
        matrix[0,3] = data.position.x
        matrix[1,3] = data.position.y
        matrix[2,3] = data.position.z
        matrix[3,3] = 1
        
        r = R.from_quat([data.orientation.x, data.orientation.y, data.orientation.z, data.orientation.w])
        rot_mat = r.as_matrix()
        matrix[0:3,0:3] = rot_mat
        matrix[3,3] = 1
        
        # write out config file for nerf 
        nerf_info = {
            "camera_type": "perspective",
            "render_height": 480,
            "render_width": 640,
            "camera_path": [
                {
                    "fov": 70,
                    "aspect": 1.33333333333
                }
            ],
            "fps": 24,
            "seconds": 4,
            "smoothness_value": 0.5,
            "is_cycle": "false",
            "crop": None
        }
        
        nerf_info['camera_path'][0]['camera_to_world'] = matrix.tolist()
        json_object = json.dumps(nerf_info, indent=4)
        
        path = "/workspace/ros2_iron/src/nerf_sim/generated_materials/camera_path_depth.json"
        with open(path, "w") as outfile:
            outfile.write(json_object)

        self.img_depth = self.render.run(['depth'], path)
        self.img_depth *= 255
        self.img_depth = cv2.cvtColor(self.img_depth, cv2.COLOR_RGB2BGR)

    
    def timer_callback(self):
        if self.img_depth is None:
            return
        
        msg_rgb = Image()
        a = np.array(self.img_depth, dtype='i1')
        msg_rgb = self.bridge.cv2_to_imgmsg(a)
        self.publisher_rgb.publish(msg_rgb)
        self.img_depth = None


def main(args=None):
    rclpy.init(args=args)

    minimal_publisher = DepthGenerator()

    rclpy.spin(minimal_publisher)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    minimal_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()