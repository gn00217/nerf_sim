import json
import argparse
import mediapy as media
import numpy as np
import torch

from pathlib import Path
from typing import Any, Dict, List, Literal, Optional, Union

from rich.progress import (
    track
)

from nerfstudio.cameras.camera_paths import (
    get_path_from_json # TODO: remove for ros
)

from nerfstudio.cameras.cameras import Cameras
from nerfstudio.pipelines.base_pipeline import Pipeline

from nerfstudio.utils import colormaps
from nerfstudio.utils.eval_utils import eval_setup

def _render_trajectory_video(
    pipeline: Pipeline,
    cameras: Cameras,
    output_filename: Path,
    rendered_output_names: List[str],
    crop_data = None,
    rendered_resolution_scaling_factor: float = 1.0,
    seconds: float = 5.0,
    output_format: Literal["images", "video"] = "video",
    image_format: Literal["jpeg", "png"] = "png",
    jpeg_quality: int = 100,
    colormap_options: colormaps.ColormapOptions = colormaps.ColormapOptions(),
) -> None:
    """Helper function to create a video of the spiral trajectory.

    Args:
        pipeline: Pipeline to evaluate with.
        cameras: Cameras to render.
        output_filename: Name of the output file.
        rendered_output_names: List of outputs to visualise.
        crop_data: Crop data to apply to the rendered images.
        rendered_resolution_scaling_factor: Scaling factor to apply to the camera image resolution.
        seconds: Length of output video.
        output_format: How to save output data.
        colormap_options: Options for colormap.
    """
    cameras.rescale_output_resolution(rendered_resolution_scaling_factor)
    cameras = cameras.to(pipeline.device)
    fps = len(cameras) / seconds

    output_image_dir = output_filename.parent / output_filename.stem
    if output_format == "images":
        output_image_dir.mkdir(parents=True, exist_ok=True)
        writer = None

        for camera_idx in track(range(cameras.size), description=""):
            aabb_box = None
            camera_ray_bundle = cameras.generate_rays(camera_indices=camera_idx, aabb_box=aabb_box)
                
            with torch.no_grad():
                outputs = pipeline.model.get_outputs_for_camera_ray_bundle(camera_ray_bundle)

            render_image = []
            for rendered_output_name in rendered_output_names:
                output_image = outputs[rendered_output_name]
                output_image = (
                    colormaps.apply_colormap(
                        image=output_image,
                        colormap_options=colormap_options,
                    )
                    .cpu()
                    .numpy()
                )
                render_image.append(output_image)
            render_image = np.concatenate(render_image, axis=1)
            if output_format == "images":
                return render_image
                
class RenderCameraPath():
    image_format: Literal["jpeg", "png"] = "jpeg"
    jpeg_quality: int = 100
    downscale_factor: float = 1.0
    eval_num_rays_per_chunk: Optional[int] = None
    colormap_options: colormaps.ColormapOptions = colormaps.ColormapOptions()
    load_config: Path
    output_path: Path = Path("renders/output.mp4")
    output_format: Literal["images", "video"] = "images"
    rendered_output_names: List[str] = []
    
    def __init__(self):
        self.load_config = Path('/vol/research/K9/test-colmap/65904bca-e8fd-11ed-a05b-0242ac120003/nerfacto/65904bca-e8fd-11ed-a05b-0242ac120003/config.yml')
        _, self.pipeline, _, _ = eval_setup(
            self.load_config,
            eval_num_rays_per_chunk=self.eval_num_rays_per_chunk,
            test_mode="inference",
        )

    def run(self, render_output_names: List[str] = [], camera_path_filename=Path("renders/camera_path.json")):
        pipeline = self.pipeline
        camera_path_filename = Path(camera_path_filename)
        with open(camera_path_filename, "r", encoding="utf-8") as f:
                camera_path = json.load(f)
        seconds = camera_path["seconds"]
        crop_data = None
        camera_path = get_path_from_json(camera_path)        
        self.rendered_output_names = render_output_names
        
        render_image = _render_trajectory_video(
            pipeline,
            camera_path,
            output_filename=self.output_path,
            rendered_output_names=self.rendered_output_names,
            rendered_resolution_scaling_factor=1.0 / self.downscale_factor,
            crop_data=crop_data,
            seconds=seconds,
            output_format=self.output_format,
            image_format=self.image_format,
            jpeg_quality=self.jpeg_quality,
            colormap_options=self.colormap_options,
        )
        return render_image

