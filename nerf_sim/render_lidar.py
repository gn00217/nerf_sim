import torch
import math
import random
from dataclasses import dataclass, field
from typing import Callable, Dict, Literal, Optional, Tuple, Union, overload

from pathlib import Path
import open3d as o3d
import argparse
import torch
import numpy as np
from jaxtyping import Float, Int, Shaped
from torch import Tensor
from nerfstudio.utils.eval_utils import eval_setup
from nerfstudio.model_components.ray_samplers import PDFSampler, UniformSampler

from nerfstudio.utils.tensor_dataclass import TensorDataclass
from nerfstudio.cameras.rays import RayBundle, RaySamples, Frustums


class RenderLiDAR():
    eval_num_rays_per_chunk: Optional[int] = None
    def __init__(self):
        self.load_config = Path('/vol/research/K9/test-colmap/65904bca-e8fd-11ed-a05b-0242ac120003/nerfacto/65904bca-e8fd-11ed-a05b-0242ac120003/config.yml')
        _, self.pipeline, _, _ = eval_setup(
            self.load_config,
            eval_num_rays_per_chunk=self.eval_num_rays_per_chunk,
            test_mode="inference",
        )
        
    def generate_pointcloud(self,ray_bundle, outputs):
        rgbs = outputs['rgb']
        points = ray_bundle.origins + ray_bundle.directions * outputs['depth']
        
        pcd = o3d.geometry.PointCloud()
        pcd.points = o3d.utility.Vector3dVector(points.double().cpu().numpy())
        pcd.colors = o3d.utility.Vector3dVector(rgbs.detach().double().cpu().numpy())
        
        return pcd
    
    def generate_direction(self):
        min, max = -30.67, 10.67 # view range degrees
        no_channels = 32
        delta_range = (max - min) / no_channels # delta degrees
        
        elevations = [math.radians(i) for i in np.arange(min, max, delta_range)]
        azimuths = [math.radians(i) for i in np.arange(0, 360, 1)]

        rays = []
        for elevation in elevations:
            for azimuth in azimuths:
                rays.append(torch.tensor([
                            math.cos(elevation) * math.sin(azimuth),
                            math.cos(elevation) * math.cos(azimuth),
                            math.sin(elevation)
                        ], device='cuda:0'))

        return torch.stack(rays)    

    def run(self, origin):
        cuda0 = torch.device('cuda:0')
    
        directions = self.generate_direction()
        origin = torch.tensor(origin, device=cuda0)
        
        x,y = directions.shape
        origins = origin.repeat(x,1)
        
        area = torch.tensor([0.0001], device=cuda0)
        pixel_area = area.repeat(x, 1)
        
        camera_indices = torch.tensor([0], device=cuda0)
    
        ray_bundle = RayBundle(origins, directions, pixel_area, camera_indices=camera_indices, nears=None, fars=None)
        outputs = self.pipeline.model(ray_bundle)
        
        pcd = self.generate_pointcloud(ray_bundle, outputs)
        
        torch.cuda.empty_cache()
        tpcd = o3d.t.geometry.PointCloud.from_legacy(pcd)
        tpcd.point.colors = (tpcd.point.colors * 255).to(o3d.core.Dtype.UInt8)  # type: ignore
        return o3d.core.Tensor.numpy(tpcd.point.positions)