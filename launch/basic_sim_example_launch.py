from launch import LaunchDescription
from launch_ros.actions import Node

import os
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():
    rviz_config_dir = os.path.join(get_package_share_directory(
        'nerf_sim'), 'config', 'config.rviz')
    print(rviz_config_dir)
    assert os.path.exists(rviz_config_dir)
    
    return LaunchDescription([
        Node(
            package='nerf_sim',
            executable='sim_depth_generator',
            name='sim_depth_generator'
        ),
        Node(
            package='nerf_sim',
            executable='sim_rgb_generator',
            name='sim_rgb_generator'
        ),
        Node(
            package='nerf_sim',
            executable='sim_pcd_generator',
            name='sim_pcd_generator'
        ),
        Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', rviz_config_dir]
        )
    ])