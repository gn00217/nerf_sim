from setuptools import find_packages, setup
import os
from glob import glob
package_name = 'nerf_sim'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        # During installation, we need to copy the launch files
        (os.path.join('share', package_name, "launch"), glob('launch/*.launch.py')),
        # Same with the RViz configuration file.
        (os.path.join('share', package_name, "config"), glob('config/*')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='root',
    maintainer_email='root@todo.todo',
    description='TODO: Package description',
    license='Apache-2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'pose_generator = nerf_sim.pose_generator:main',
            'sim_rgb_generator = nerf_sim.sim_rgb_generator:main',
            'sim_depth_generator = nerf_sim.sim_depth_generator:main',
            'sim_pcd_generator = nerf_sim.sim_pcd_generator:main',
        ],
    },
)
